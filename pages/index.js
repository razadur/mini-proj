import Navbar from '../components/Navbar';
import Dashboard from '../components/Dashboard';
import Map from '../components/Map';
import "../scss/style.scss";
const Index = () => (
  <div>
    <Navbar/>
    <Dashboard/>
    <Map/>
  </div>
);
export default Index;