import Document, {Head, Main, NextScript} from 'next/document';

export default class MyDocument extends Document{
    render(){
        return(
            <html>
                <Head>
                    <title>AG Demo Project</title>
                    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@tailwindcss/ui@latest/dist/tailwind-ui.min.css"></link>
                    <script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
                    <link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet" />
                </Head>
                <body>
                    <Main/>
                    <NextScript/>
                    <div className="footer">
                        <p>{(new Date().getFullYear())} AG demo Project. </p>
                    </div>
                </body>
            </html>
        );
    }
}