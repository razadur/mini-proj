import React, { Component} from 'react';
import MapGL from 'react-map-gl';
const MAPBOX_TOKEN = 'pk.eyJ1Ijoia2t1cnR6IiwiYSI6ImNrOWE4djN1eDAyd3UzZXBuYzF1OWtjYTQifQ.obmfLm5bA5yqrac86Fw8GQ';

class Map extends Component{
    constructor(props) {
        super(props);
        this.state = {
          viewport: {
            latitude: 40,
            longitude: -100,
            zoom: 3
          }
        };
      }

    componentDidMount(){
        const map = this.reactMap.getMap();
        map.on('load', function() {
            map.addSource('admin-1-states', {
                'type': 'geojson',
                'data':
                'https://raw.githubusercontent.com/shawnbot/topogram/master/data/us-states.geojson'
                //  given geojson
                // 'https://d2ad6b4ur7yvpq.cloudfront.net/naturalearth-3.3.0/ne_110m_admin_1_states_provinces_shp.geojson'
            });

            map.addLayer(
                {
                'id': 'admin-1-states-line',
                'type': 'line',
                'source': 'admin-1-states',
                'layout': {},
                'paint': {
                'line-color': '#f09',
                'line-opacity': 1
                }
            });
        });
      }

    render(){
        return(
            <main>
                <div className="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
                    <div className="px-4 py-6 sm:px-0">
                        <div className="border-4 border-dashed border-gray-200 rounded-lg h-96">
                            <MapGL
                                ref={(reactMap) => this.reactMap = reactMap}
                                {...this.state.viewport}
                                width="93.5vw"
                                height="56.5vh"
                                mapStyle="mapbox://styles/mapbox/streets-v11"
                                onViewportChange={viewport => this.setState({viewport})}
                                mapboxApiAccessToken={MAPBOX_TOKEN}
                            />
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}
    
export default Map;